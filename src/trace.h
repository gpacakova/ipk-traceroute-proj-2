/* IPK Project 2 - Traceroute
	Author: xpacak01
*/
#ifndef TRACE_H
#define TRACE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/ip_icmp.h>
#include <netinet/ip.h>
#include <unistd.h>
#include <linux/errqueue.h>
#include <netdb.h> //DNS lookup and hostname to IP conversion

#define FIRST_TTL 1
#define MAX_TTL 30
#define BUFFLEN 1024
#define PORT 33434

typedef struct{
    int first_ttl;
    int max_ttl;
    int ip_version;
    char *ip_adress;
} Params;

typedef struct{
	struct sockaddr_in __Self_4;
	struct sockaddr_in __Other_4;
	struct sockaddr_in6 __Self_6;
	struct sockaddr_in6 __Other_6;
} Family;

void __params_init(int ver, int bytes);
void __check_args(int argc, char *argv[]);
void __error(int code, char *msg);
void __print_help();
void __trace__(int __Family);
struct addrinfo *__hostname_to_ip(char *hostname);
char *__inverse_lookup(char *ip, size_t mem);
double __get_time ();

#endif