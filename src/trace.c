/* IPK Project 2 - Traceroute
	Author: xpacak01
*/
#include "trace.h"

Params opts;
Family __addr;

/////////////////MAIN///////////////////////
int main(int argc, char *argv[]){
    __check_args(argc,argv); //checks args
    fflush(stdout);
    //calls for trace, depending on the IP version
    opts.ip_version == 4 ? __trace__(AF_INET) : __trace__(AF_INET6);
    return 0;
}

/*********** struct Params ************/
//initializes Params global structure
void __params_init(int ver, int bytes){
    opts.first_ttl=FIRST_TTL;
    opts.max_ttl=MAX_TTL;
    opts.ip_version=ver;
    opts.ip_adress = calloc((size_t)bytes, bytes * sizeof(char));
}

/***************** ARGS *****************************/
//checks arguments
void __check_args(int argc, char *argv[]){
    struct addrinfo *ip_type = NULL;
    switch(argc){
        case 2: //only IP must be supplied
            ip_type = __hostname_to_ip(argv[1]);
            if (ip_type == NULL)
                __error(2, "Unknown name or service.");
            break;
        case 4: // -f or -m
            if(strcmp(argv[1], "-f") == 0){
                if(!isdigit(*argv[2])){
                    __error(1, "Bad parameters.");
                }
                ip_type = __hostname_to_ip(argv[3]);
                if (ip_type == NULL)
                    __error(2, "Unknown name or service.");
                opts.first_ttl = strtol(argv[2], NULL, 10);
            }
            else if(strcmp(argv[1], "-m") == 0){
                if(!isdigit(*argv[2]))
                    __error(1, "Bad parameters");
                ip_type = __hostname_to_ip(argv[3]);
                if (ip_type == NULL)
                        __error(2, "Unknown name or service.");
                opts.max_ttl = strtol(argv[2], NULL, 10);
            }
            else{
                __error(1, "Bad parameters.");
            }
            break;
        case 6: //-f and -m
            if(strcmp(argv[1], "-f") == 0 && strcmp(argv[3], "-m") == 0){
                if(!isdigit(*argv[2]) || !isdigit(*argv[4]))
                    __error(1, "Bad parameters.");
                ip_type = __hostname_to_ip(argv[5]);
                if (ip_type == NULL)
                    __error(2, "Unknown name or service.");
                opts.first_ttl = strtol(argv[2], NULL, 10);
                opts.max_ttl = strtol(argv[4], NULL, 10);
            }
            else if(strcmp(argv[1], "-m") == 0 && strcmp(argv[3], "-f") == 0){ //-m and -f
                if(!isdigit(*argv[2]) || !isdigit(*argv[4]))
                    __error(1, "Bad parameters.");
                ip_type = __hostname_to_ip(argv[5]);
                if (ip_type == NULL)
                    __error(2, "Unknown name or service.");
                opts.first_ttl = strtol(argv[4], NULL, 10);
                opts.max_ttl = strtol(argv[2], NULL, 10);
            }
            else{
                __error(1, "Bad parameters.");
            }
            break;
        default:
            __print_help();
            exit(0);
    }
    freeaddrinfo(ip_type);
}

/**************HOSTNAME TO IP ***********/
/* Converts hostname to IP adress if possible*/
/* Detects IP version */
struct addrinfo *__hostname_to_ip(char *hostname)
{
    struct addrinfo hints;
    struct addrinfo *servinfo;
    struct sockaddr_in *h4;
    struct sockaddr_in6 *h6;
    char ip4[INET_ADDRSTRLEN];
    char ip6[INET6_ADDRSTRLEN];

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;

    if ((getaddrinfo(hostname , "33434" , &hints , &servinfo)) != 0)
        return NULL;

    if(servinfo->ai_family == AF_INET){
        h4 = (struct sockaddr_in *) servinfo->ai_addr;
        inet_ntop(servinfo->ai_family, &(h4->sin_addr), ip4, INET_ADDRSTRLEN);
        __params_init(4, INET_ADDRSTRLEN);
        strcpy(opts.ip_adress, ip4);
    }
    else if(servinfo->ai_family == AF_INET6){
        h6 = (struct sockaddr_in6 *) servinfo->ai_addr;
        inet_ntop(servinfo->ai_family, &(h6->sin6_addr), ip6, INET6_ADDRSTRLEN);
        __params_init(6, INET6_ADDRSTRLEN);
        strcpy(opts.ip_adress, ip6);
    }
    return servinfo;
}

/*********** iNVERSE LOOKUP*************/
/* DNS lookup - IP back to hostname, if possible */
char *__inverse_lookup(char *ip, size_t mem){ //should work for both v4 and v6
    struct addrinfo *result;
    struct addrinfo *res;
    int error;
    /* resolve the domain name into a list of addresses */
    error = getaddrinfo(ip, NULL, NULL, &result);
    if (error != 0){
        freeaddrinfo(result);
        return NULL;
    }
    /* loop over all returned results and do inverse lookup */
    for (res = result; res != NULL; res = res->ai_next)
    {
        char *hostname = calloc(mem, mem * sizeof(char));

        error = getnameinfo(res->ai_addr, res->ai_addrlen, hostname, NI_MAXHOST, NULL, 0, 0);
        if (error != 0)
            continue;

        if (strcmp(hostname, "\0") != 0){
            freeaddrinfo(result);
            return hostname;
        }
        free(hostname);
    }
    freeaddrinfo(result);
    return NULL;

}
/**********TIME*********************/
/* Gets the time in ms with the most possible precision */
double __get_time () {
    struct timeval tv;
    double d;

    gettimeofday (&tv, NULL);
    d = ((double) tv.tv_usec) / 1000. + (unsigned long) tv.tv_sec * 1000;

    return d;
}
/************** TRACE ******************/
/* Begins trace , depends on the IP version */
void __trace__(int __Family){
    int __Socket;
    bool __dest_reached = 0;
    ssize_t recv_size = 0;
    char buf[BUFFLEN];
    int val = 1;
    struct msghdr msg;
    struct iovec iov;
    struct cmsghdr *cmsg;
    struct icmphdr *icmph;
    struct icmphdr recvh;
    double t_send;
    double t_recv;
    int first_ttl = opts.first_ttl;
    int max_ttl = opts.max_ttl;

    /** create UDP socket **/ /*IPV4*/
    if(__Family == AF_INET){

        if ((__Socket=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
            __error(4, "UDP socket creation failed.");

        memset((char *) &__addr.__Self_4, 0, sizeof(__addr.__Self_4));
        memset((char *) &__addr.__Other_4, 0, sizeof(__addr.__Other_4));

        __addr.__Self_4.sin_family = AF_INET;
        __addr.__Self_4.sin_port = htons(PORT);

        __addr.__Other_4.sin_family = AF_INET;
        __addr.__Other_4.sin_port = htons(PORT);
        inet_pton(AF_INET, opts.ip_adress, &__addr.__Other_4.sin_addr.s_addr);

        /*****SOCKOPT ***/
        if (setsockopt(__Socket, IPPROTO_IP, IP_TTL, (const char*)&first_ttl, sizeof(first_ttl)))
                __error(4, "Error setting TTL");



        if (setsockopt(__Socket, SOL_IP, IP_RECVERR, &val, sizeof(val)))
                __error(4, "Error setting RECVERR");

    }
    else{ ///IPV6
        // create socket
        if ((__Socket=socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP)) == -1)
            __error(6, "UDP socket creation failed.");

        memset((char *) &__addr.__Self_6, 0, sizeof(__addr.__Self_6));
        memset((char *) &__addr.__Other_6, 0, sizeof(__addr.__Other_6));

        __addr.__Self_6.sin6_family = AF_INET6;
        __addr.__Self_6.sin6_port = htons(PORT);

        __addr.__Other_6.sin6_family = AF_INET6;
        __addr.__Other_6.sin6_port = htons(PORT);
        inet_pton(AF_INET6, opts.ip_adress, &__addr.__Other_6.sin6_addr.s6_addr);

        /*****SOCKOPT ***/
        if (setsockopt(__Socket, IPPROTO_IPV6, IPV6_UNICAST_HOPS, (const char*)&first_ttl, sizeof(first_ttl)))
                __error(6, "Error setting TTL");



        if (setsockopt(__Socket, SOL_IPV6, IPV6_RECVERR, &val, sizeof(val)))
                __error(6, "Error setting RECVERR");
    }

    //Sets ICMP echo message to be sent
    icmph = (struct icmphdr *)malloc(sizeof(struct icmphdr));
    icmph->type = ICMP_ECHO;
    icmph->code = 0;
    icmph->un.echo.id = (u_int16_t )getpid();

    char Prev4[INET_ADDRSTRLEN]="";
    char Prev6[INET6_ADDRSTRLEN]="";
    ///// BEGINNING of outer loop -> iterates over TTL
    while(first_ttl <= max_ttl && __dest_reached == 0){
        int duplicate = 0;
        //Prepares the msg struct
        memset(buf, 0, BUFFLEN);
        iov.iov_base = &recvh;
        iov.iov_len = sizeof(recvh);
        if(__Family == AF_INET){ //Ipv4
            msg.msg_name = &__addr.__Self_4;
            msg.msg_namelen = sizeof(__addr.__Self_4);

        }
        else{ //Ipv6
            msg.msg_name = &__addr.__Self_6;
            msg.msg_namelen = sizeof(__addr.__Self_6);
        }
        msg.msg_iov = &iov;
        msg.msg_iovlen = 1;
        msg.msg_flags = 0;
        msg.msg_control = buf;
        msg.msg_controllen = sizeof(buf);

        if(__Family == AF_INET){ //IPv4 TTL setting + sendto
            if (setsockopt(__Socket, SOL_IP, IP_TTL, (const char*)&first_ttl, sizeof(first_ttl)))
            __error(4, "Error setting TTL");

            for(int i = 0; i < 3; i++){ //sending 3 packets with every TTL, in case 1 gets lost :(
                if(sendto(__Socket, (char *)icmph, sizeof(struct icmphdr), 0, (struct sockaddr *)&__addr.__Other_4, sizeof(__addr.__Other_4))<0){
                    if(errno == ENETUNREACH){
                        __error(4, "Network unreachable. Check your internet connection.");
                    }
                }
            }

        }

        else{ //IPv6 TTL setting + sendto
            if (setsockopt(__Socket, IPPROTO_IPV6, IPV6_UNICAST_HOPS, (const char*)&first_ttl, sizeof(first_ttl)))
                __error(6, "Error setting TTL");

            for(int i = 0; i < 3; i++){ //3 packets
                if(sendto(__Socket, (char *)icmph, sizeof(struct icmphdr), 0, (struct sockaddr *)&__addr.__Other_6, sizeof(__addr.__Other_6))<0){
                    if(errno == ENETUNREACH){
                        __error(6, "Network unreachable. Check your internet connection and whether your provider supports IPv6.");
                    }
                }
            }
        }
        t_send = __get_time(); //time at which the echo was sent

        /* Inner loop -> recieves response and checks ICMP messages */
        while(1){
            t_recv = __get_time(); //2 sec count
            double elapsedtime = t_recv - t_send; //elapsed time in ms

            recv_size = recvmsg(__Socket, &msg, MSG_ERRQUEUE); //RECVMSG

            if (recv_size < 0) {
                if(elapsedtime > 2000){ // TIMEOUT after 2s
                    fprintf(stdout, "%d   * * *\n", first_ttl);
                    break;
                }
            continue;
            }
            //time at which we actually got something
            t_recv = __get_time();
            elapsedtime = t_recv - t_send;

            /* Checks for ICMP messages in recieved response*/
            for (cmsg = CMSG_FIRSTHDR(&msg);cmsg; cmsg = CMSG_NXTHDR(&msg, cmsg))
            {
                /* IPv4 messages */
                if (cmsg->cmsg_level == SOL_IP && cmsg->cmsg_type == IP_RECVERR){
                    struct sock_extended_err *e = (struct sock_extended_err*) CMSG_DATA(cmsg);

                    if (e && e->ee_origin == SO_EE_ORIGIN_ICMP) {
                        struct sockaddr_in *sin = (struct sockaddr_in *)(e+1);

                        char __IP[INET_ADDRSTRLEN];
                        char *__Host;
                        inet_ntop(AF_INET, &sin->sin_addr, __IP, INET_ADDRSTRLEN);
                        if(strcmp(__IP, Prev4) == 0){
                            //throws away more responses from the same IP
                            duplicate++;
                            continue;
                        }

                        __Host = __inverse_lookup(__IP, NI_MAXHOST);

                        char __UN[3] = "\0\0\0";
                        int flag =0;
                        if(__Host == NULL){
                            __Host = __IP;
                            flag = 1;
                        }

                        switch(e->ee_type){
                            case ICMP_TIME_EXCEEDED:
                                fprintf(stdout, "%d   %s (%s)   %.3f ms    ", first_ttl, __Host, __IP, elapsedtime);
                                break;
                            case ICMP_DEST_UNREACH:
                                if(e->ee_code == 0){ //network unreach
                                    strcpy(__UN, "N!");
                                }
                                else if(e->ee_code == 1){ //host unreach
                                    strcpy(__UN, "H!");
                                }
                                else if(e->ee_code == 2){ //port unreach
                                    strcpy(__UN, "P!");
                                }
                                else if(e->ee_code == 13){ //communication administr. prohibited
                                    strcpy(__UN, "X!");
                                }
                                fprintf(stdout, "%d   %s (%s)   ", first_ttl, __Host, __IP);
                                if(strcmp(__UN, "\0\0\0") == 0)
                                    fprintf(stdout, "%.3f ms", elapsedtime);
                                else
                                    fprintf(stdout, "%s", __UN);
                                __dest_reached = 1;
                                break;
                            default:
                                fprintf(stdout, "%d   * * * <unspecified ICMP error>", first_ttl);
                        }
                        fprintf(stdout, "\n");
                        if(flag == 0){
                            free(__Host);
                        }
                        strcpy(Prev4, __IP);
                    }
                } /* IPv6 messages */
                else if(cmsg->cmsg_level == SOL_IPV6 && cmsg->cmsg_type == IPV6_RECVERR){
                    struct sock_extended_err *e = (struct sock_extended_err*) CMSG_DATA(cmsg);
                    if (e && e->ee_origin == SO_EE_ORIGIN_ICMP6){
                        struct sockaddr_in6 *sin = (struct sockaddr_in6 *)(e+1);
                        char __IP[INET6_ADDRSTRLEN];
                        char *__Host;
                        inet_ntop(AF_INET6, &sin->sin6_addr, __IP, INET6_ADDRSTRLEN);
                        if(strcmp(__IP, Prev6) == 0){
                            //throws away more responses from the same IP
                            duplicate++;
                            continue;
                        }
                        __Host = __inverse_lookup(__IP, NI_MAXHOST);
                        int flag =0;
                        if(__Host == NULL){
                            __Host = __IP;
                            flag = 1;
                        }
                        char __UN[3] = "\0\0\0";
                        switch(e->ee_type){
                            case 3:
                                fprintf(stdout, "%d   %s (%s)   %.3f ms    ", first_ttl, __Host, __IP, elapsedtime);
                                break;
                            case 1:
                                if(e->ee_code == 0){ //no route to dest
                                    strcpy(__UN, "N!");
                                }
                                else if(e->ee_code == 3){ // adress unreach
                                    strcpy(__UN, "H!");
                                }
                                else if(e->ee_code == 1){ //comm admin prohibited
                                    strcpy(__UN, "X!");
                                }
                                else if(e->ee_code == 7){ //error in header
                                    strcpy(__UN, "P!");
                                }
                                fprintf(stdout, "%d   %s (%s)   ", first_ttl, __Host, __IP);
                                if(strcmp(__UN, "\0\0\0") == 0)
                                    fprintf(stdout, "%.3f ms", elapsedtime);
                                else
                                    fprintf(stdout, "%s", __UN);
                                __dest_reached = 1;
                                break;
                            default:
                                fprintf(stdout, "%d   * * * <unspecified ICMP error>", first_ttl);
                        }
                        fprintf(stdout, "\n");
                        if(flag == 0){
                            free(__Host);
                        }
                        strcpy(Prev6, __IP);
                    }
                }

            }
            break;
        }
        first_ttl = first_ttl + 1 - duplicate; //Increment TTL!
    }
    free(icmph);
    free(opts.ip_adress);
    close(__Socket);
}

/************ ERROR & HELP ******************/
void __error(int code, char *msg){
    fflush(stdout);
    fprintf(stderr, "ERROR: %s\n", msg);
    exit(code);
}

void __print_help(){
    fprintf(stdout, "Author: Gabriela Pacakova, xpacak01\n"
            "Date: Apr 23, 2017\n"
            "Program: IPK - Traceroute\n"
            "Usage: ./trace [-f first_ttl] [-m max_ttl] <ip-address> \n"
            "\t[-f first_ttl] - Specifies the TTL value for the first packet. Default is 1.\n"
            "\t[-m max_ttl] - Specifies max TTL value. Default is 30.\n"
            "\t<ip-address> - IPv4/IPv6 adress of the target.\n");
}