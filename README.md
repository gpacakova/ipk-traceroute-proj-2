Maly traceroute program, co vie robit aj s IPv6.
Impementovane aj s rozsirenim o preklad DNS.
See trace.1 for more info.

Pozn.: Nasledujuce zdrojove kody sluzia ako inspiracia a pomoc, pre pripad, ze sa sami neviete pohnut dalej. Prosim berte do uvahy, ze plagiatorstvo sa hodnoti 0 bodmi, neudelenim zapoctu a dalsim adekvatnym postihom podla platneho disciplinarneho radu VUT. V preklade: Skuste v prvom rade pouzit vlastnu hlavu a rozsirit svoj obzor o nove vedomosti. Koniec koncov to je jeden z dovodov preco chodime na FIT.

PS: Nasledujuce riesenie nemusi byt bezchybne alebo/ani optimalne.